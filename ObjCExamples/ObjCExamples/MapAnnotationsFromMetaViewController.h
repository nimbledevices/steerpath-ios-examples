//
//  MapAnnotationsFromMetaViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapAnnotationsFromMetaViewController Class Definition

/**
    This class implements a map with annotations that are based on
    point of interest data downloaded from SPMetaService.
*/
@interface MapAnnotationsFromMetaViewController : UIViewController

@end

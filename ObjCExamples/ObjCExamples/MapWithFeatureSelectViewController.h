//
//  MapWithFeatureSelectViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 08/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithFeatureSelectViewController Class Definition

/**
    This class implements a map and adds annotation onto map when
    a feature with 'localRef' is clicked if that feature has
    a corresponding point of interest in SPMetaService.
*/
@interface MapWithFeatureSelectViewController : UIViewController

@end

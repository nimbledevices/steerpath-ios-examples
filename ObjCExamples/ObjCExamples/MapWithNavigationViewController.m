//
//  MapWithNavigationViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 20/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithNavigationViewController.h"
@import Steerpath;

#pragma mark - MapWithNavigationViewController Private Interface

@interface MapWithNavigationViewController ()<SPLocationManagerDelegate, SPMapViewDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - MapWithNavigationViewController Class Implementation

@implementation MapWithNavigationViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.220946, 24.812392);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 19.0f animated: NO];
    
    /**
        Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
        If bluetooth is not on, the app will ask the user to turn on bluetooth.
        Also asks permission to use iOS Location Services (GPS).
     
        When users position changes, a callback will be made to the delegate method
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
    */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
    
    /**
        For the sake of this example we're using a hardcoded test location.
    */
    SPLocation* testLocation = [[SPLocation alloc] initWithCoordinate: CLLocationCoordinate2DMake(60.22089814, 24.81234131) floor: 2 horizontalAccuracy: 1 course: 0 source: SPLocationSourceBluetooth];
    testLocation.building = @"67";  //building identifier
    [self.locationManager setDebugLocation: testLocation];
    [self.mapView setCurrentLocation: testLocation];
    
    
    /**
        Simple example:
        - Fetch all toilets on floor 2
    */
    SPQuery* query = [SPQuery new];
    query.floor = @(2);
    query.tags = @[@"Toilet"];
    
    /**
        Requesting point of interest data from Steerpath Meta Service. Requires internet
        connection when first time downloading a particular set of data. Will cache data locally.
     
        To use the SPMetaService you must have a Steerpath API key.
        See SPAccountManager.h for more information.
    */
    __weak typeof(self) wself = self;
    [[SPMetaService sharedInstance] getPointsOfInterest: query completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        __strong typeof(wself) sself = wself;
        if (sself) {
            if (points.count > 0) {
                SPGeoJson* firstToilet = points.firstObject;
                if (firstToilet.floor && CLLocationCoordinate2DIsValid(firstToilet.coordinate)) {
                    
                    //Add first toilet annotation to map
                    MGLPointAnnotation* pointAnnotation = [MGLPointAnnotation new];
                    pointAnnotation.coordinate = firstToilet.coordinate;
                    pointAnnotation.title = @"Toilet";
                    [sself.mapView addAnnotation: pointAnnotation floor: firstToilet.floor];
                    
                    SPLocation* targetLocation = firstToilet.location;
                    targetLocation.info = @"Toilet";    //this will be shown in the navigation instructions list
                    [sself.mapView startNavigationTo: targetLocation];
                }
            }
        }
    }];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    [self.mapView setCurrentLocation: location];
}

#pragma mark - SPMapViewDelegate

/**
    This determines the alpha for the route line among other things.
*/
-(CGFloat)mapView:(MGLMapView *)mapView alphaForShapeAnnotation:(MGLShape *)annotation {
    return 1.0f;
}

/**
    This determines the color of the route line among other things.
*/
-(UIColor *)mapView:(MGLMapView *)mapView strokeColorForShapeAnnotation:(MGLShape *)annotation {
    return [UIColor blueColor]; //blue route
}

/**
    This can be used to determine the width of the route line on the map.
*/
-(CGFloat)mapView:(MGLMapView *)mapView lineWidthForPolylineAnnotation:(MGLPolyline *)annotation {
    return 6.0f;
}

/**
    This will create an annotation view.
    The map view will internally add annotations onto the route so you
    will need to implement this if you want to use the route annotations provided by the SDK.
*/
-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    [annotationView setAnnotationColor: [UIColor blueColor]];   //You can change color of the annotations like this
    return annotationView;
}

-(void)mapView:(SPMapView*)mapView navigationDidBegin:(SPRoute*)route {
    NSLog(@"This is called when navigation begins");
}

-(void)mapView:(SPMapView*)mapView navigationDidEnd:(SPRoute*)route {
    NSLog(@"This is called when navigation ends");
}

-(void)mapView:(SPMapView*)mapView navigationFailedWithError:(NSError*)error {
    NSLog(@"This is called when navigation fails");
}

-(void)mapView:(SPMapView*)mapView didNavigateToLocation:(SPLocation*)location {
    NSLog(@"This is called when user has reached a destination");
}

-(void)mapView:(SPMapView*)mapView willRecalculateRouteFrom:(SPLocation*)location {
    NSLog(@"This is called when user goes too far away from route and a new route will be calculated.");
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

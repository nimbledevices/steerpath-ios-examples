//
//  MapWithRouteViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 20/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithRouteViewController.h"
@import Steerpath;

#pragma mark - MapWithRouteViewController Private Interface

@interface MapWithRouteViewController ()<SPLocationManagerDelegate, SPMapViewDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - MapWithRouteViewController Class Implementation

@implementation MapWithRouteViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // Center map around the building
    
    SPQuery *buildingQuery = SPQuery.new;
    
    // Get list of building for current API Key
    
    [[SPMetaService sharedInstance] getBuildings:buildingQuery completion:^(NSArray<SPGeoJson *> * _Nullable buildings, NSError * _Nullable err) {
        if (buildings) {
            SPGeoJson *firstBuilding = buildings.firstObject;
            
            if (firstBuilding) {
                SPBoundingBox *boundingBox = [firstBuilding boundingBox];
                MGLCoordinateBounds mglBoundingBox = MGLCoordinateBoundsMake([boundingBox southWest], [boundingBox northEast]);
                UIEdgeInsets edgeInsets = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
                
                [[self mapView] setVisibleCoordinateBounds:mglBoundingBox edgePadding:edgeInsets animated:NO];
            }
        }
    }];
    
    /**
        Map view will start to follow users location.
    */
    [self.mapView setUserTrackingMode: MGLUserTrackingModeFollow animated: YES];
    
    /**
        Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
        If bluetooth is not on, the app will ask the user to turn on bluetooth.
        Also asks permission to use iOS Location Services (GPS).
     
        When users position changes, a callback will be made to the delegate method
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
    */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
    
    /**
        Create a start location and an end location for a route.
        Note that locations can be on different floors.
     
        These will be used for calculating routes via SPDirectionsManager.
    */
    SPLocation* startLocation = [[SPLocation alloc] initWithCoordinate: CLLocationCoordinate2DMake(60.22094595, 24.81237337) floor: 2 horizontalAccuracy: 1 course: 0 source: SPLocationSourceBluetooth];
    SPLocation* endLocation = [[SPLocation alloc] initWithCoordinate: CLLocationCoordinate2DMake(60.22098448, 24.81246399) floor: 2 horizontalAccuracy: 1 course: 0 source: SPLocationSourceBluetooth];
    /**
        Create a directions request. Note that you can pass multiple destinations.
        SPDirectionsManager will attempt to find routes for each destination.
    */
    SPDirectionsRequest* request = [[SPDirectionsRequest alloc] initWithLocation: startLocation destinations: @[endLocation]];
    
    /**
        Calculate directions (routes). This requires an internet connection if route information
        has not yet been downloaded onto device. You can however bundle route information into your
        app if necessary. For offline routes contact support@steerpath.com
     
        Currently the SDK supports only routes inside buildings.
    */
    [[SPDirectionsManager sharedInstance] getDirectionsWithRequest: request completion:^(SPDirectionsResponse * _Nullable response, NSError * _Nullable error) {
        //Check if we found any routes
        if (response.routes.count > 0) {
            /**
                Draws first route that was found onto map.
                Currently SPMapView supports only drawing a single route at a time
                and it will replace the old route if necessary.
            */
            SPRoute* route = response.routes.firstObject;
            [self.mapView setRoute: route];
        }
    }];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    
    /**
        Pass the location object to the map to show users location.
    */
    [self.mapView setCurrentLocation: location];
}

/** This is called when positioning data has been downloaded, after this method is called, bluetooth positioning will work. */
-(void)spLocationManager:(SPLocationManager *)manager didLoadNDD:(BOOL)success error:(NSError *)error {
    //If success == true, indoor positioning is available.
}

#pragma mark - SPMapViewDelegate

/**
    This determines the alpha for the route line among other things.
*/
-(CGFloat)mapView:(MGLMapView *)mapView alphaForShapeAnnotation:(MGLShape *)annotation {
    return 1.0f;
}

/**
    This determines the color of the route line among other things.
*/
-(UIColor *)mapView:(MGLMapView *)mapView strokeColorForShapeAnnotation:(MGLShape *)annotation {
    return [UIColor blueColor]; //blue route
}

/**
    This can be used to determine the width of the route line on the map.
*/
-(CGFloat)mapView:(MGLMapView *)mapView lineWidthForPolylineAnnotation:(MGLPolyline *)annotation {
    return 6.0f;
}

/**
    This will create an annotation view.
    The map view will internally add annotations onto the route so you
    will need to implement this if you want to use the route annotations provided by the SDK.
*/
-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    [annotationView setAnnotationColor: [UIColor blueColor]];   //You can change color of the annotations like this
    return annotationView;
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

//
//  MapWithPositioningViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithPositioningViewController.h"
@import Steerpath;

#pragma mark - MapWithPositioningViewController Private Interface

@interface MapWithPositioningViewController ()<SPLocationManagerDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - MapWithPositioningViewController Class Implementation

@implementation MapWithPositioningViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 14.0f animated: NO];
    
    /**
        Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
     
        When users position changes, a callback will be made to the delegate method
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
    */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    /**
        Pass the location object to the map to show users location.
    */
    [self.mapView setCurrentLocation: location];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

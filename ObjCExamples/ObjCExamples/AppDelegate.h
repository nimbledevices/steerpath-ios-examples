//
//  AppDelegate.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 15/11/2016.
//  Copyright © 2016 Steerpath. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


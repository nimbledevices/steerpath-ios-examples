//
//  ExampleMenuViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "ExampleMenuViewController.h"
#import "ExampleTableViewCell.h"
#import "Examples.h"

#pragma mark - ExampleMenuViewController Private Interface

@interface ExampleMenuViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView* exampleTableView;

@end

#pragma mark - ExampleMenuViewController Class Implementation

@implementation ExampleMenuViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UINib* menuCellNib = [UINib nibWithNibName: kExampleTableViewCellNib bundle: nil];
    [self.exampleTableView registerNib: menuCellNib forCellReuseIdentifier: kExampleTableViewCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: kExampleTableViewCellIdentifier forIndexPath: indexPath];
    if (cell) {
        if ([cell isKindOfClass: [ExampleTableViewCell class]]) {
            ExampleTableViewCell* menuCell = (ExampleTableViewCell*)cell;
            menuCell.titleLabel.text = EXAMPLE_NAME((ExampleType)indexPath.row);
        }
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ExampleTypeCount;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* segue = nil;
    ExampleType type = (ExampleType)indexPath.row;
    switch (type) {
        case ExampleTypeBasicMap:
            segue = @"ShowBasicMapView";
            break;
        case ExampleTypeMapWithPositioning:
            segue = @"ShowMapWithPositioning";
            break;
        case ExampleTypeMapWithDebugPosition:
            segue = @"ShowMapWithDebugPosition";
            break;
        case ExampleTypeMapWithRoute:
            segue = @"ShowMapWithRoute";
            break;
        case ExampleTypeMapWithNavigation:
            segue = @"ShowMapWithNavigation";
            break;
        case ExampleTypeMapWithAnnotations:
            segue = @"ShowMapWithAnnotations";
            break;
        case ExampleTypeMapWithCustomAnnotations:
            segue = @"ShowMapWithCustomAnnotations";
            break;
        case ExampleTypeMapWithFloorAnnotations:
            segue = @"ShowMapWithFloorAnnotations";
            break;
        case ExampleTypeAnnotationsFromMeta:
            segue = @"ShowAnnotationsFromMeta";
            break;
        case ExampleTypeAnnotationsWithPopup:
            segue = @"ShowAnnotationsWithPopup";
            break;
        case ExampleTypeFeatureSelection:
            segue = @"ShowFeatureSelection";
            break;
        case ExampleTypeLiveUsers:
            segue = @"ShowLiveUsers";
            break;
        case ExampleTypeBackgroundUpdates:
            segue = @"ShowBackgroundUpdates";
            break;
        default:
            break;
    }
    
    if (segue) {
        [self performSegueWithIdentifier: segue sender: self];
    }
    
}

@end

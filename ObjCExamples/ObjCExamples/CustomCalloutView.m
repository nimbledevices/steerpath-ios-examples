//
//  CustomCalloutView.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 22/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "CustomCalloutView.h"

#pragma mark - CustomPointAnnotation Class Implementation

@implementation CustomPointAnnotation
@end

#pragma mark - Constants

static CGFloat const tipHeight = 10.0;
static CGFloat const tipWidth = 10.0;

#pragma mark - CustomCalloutView Private Interface

@interface CustomCalloutView()

@property (nonatomic, strong) UILabel* textLabel;

@end

#pragma mark - CustomCalloutView Class Implementation

@implementation CustomCalloutView

@synthesize leftAccessoryView = _leftAccessoryView;
@synthesize rightAccessoryView = _rightAccessoryView;

#pragma mark - Object Lifecycle

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.textLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        self.textLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.textLabel];
    }
    return self;
}

#pragma mark - CalloutView Protocol

- (void)presentCalloutFromRect:(CGRect)rect inView:(UIView *)view constrainedToRect:(CGRect)constrainedRect animated:(BOOL)animated {
    if ([self.delegate respondsToSelector:@selector(calloutViewWillAppear:)]) {
        [self.delegate performSelector:@selector(calloutViewWillAppear:) withObject:self];
    }
    [view addSubview:self];
    
    if ([self.representedObject respondsToSelector:@selector(title)]) {
        self.textLabel.text = self.representedObject.title;
        [self.textLabel sizeToFit];
    }
    
    //Prepare text label frame
    CGFloat frameWidth = self.textLabel.bounds.size.width;
    CGFloat frameHeight = self.textLabel.bounds.size.height * 2.0;
    CGFloat frameOriginX = rect.origin.x + (rect.size.width/2.0) - (frameWidth/2.0);
    CGFloat frameOriginY = rect.origin.y - frameHeight;
    self.frame = CGRectMake(frameOriginX, frameOriginY, frameWidth, frameHeight);
    
    if ([self.delegate respondsToSelector:@selector(calloutViewDidAppear:)]) {
        [self.delegate performSelector:@selector(calloutViewDidAppear:) withObject:self];
    }
}

-(void)setCenter:(CGPoint)center {
    center.y = center.y - CGRectGetMidY(self.bounds);
    [super setCenter:center];
}

-(void)dismissCalloutAnimated:(BOOL)animated {
    if (self.superview) {
        [self removeFromSuperview];
    }
}

#pragma mark - Drawing

-(void)drawRect:(CGRect)rect {
    UIColor *fillColor = [UIColor whiteColor];
    
    CGFloat tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0);
    CGPoint tipBottom = CGPointMake(rect.origin.x + (rect.size.width / 2.0), rect.origin.y +rect.size.height);
    CGFloat heightWithoutTip = rect.size.height - tipHeight;
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGMutablePathRef tipPath = CGPathCreateMutable();
    CGPathMoveToPoint(tipPath, NULL, 0, 0);
    CGPathAddLineToPoint(tipPath, NULL, 0, heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, tipLeft, heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, tipBottom.x, tipBottom.y);
    CGPathAddLineToPoint(tipPath, NULL, tipLeft + tipWidth, heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, CGRectGetWidth(rect), heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, CGRectGetWidth(rect), 0);
    CGPathCloseSubpath(tipPath);
    
    [fillColor setFill];
    CGContextAddPath(ctx, tipPath);
    CGContextFillPath(ctx);
    CGPathRelease(tipPath);
}

@end

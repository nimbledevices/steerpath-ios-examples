//
//  MapWithCustomAnnotationsViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 23/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithCustomAnnotationsViewController.h"
#import "CustomAnnotationView.h"
#import "CustomCalloutView.h"
@import Steerpath;

#pragma mark - MapWithCustomAnnotationsViewController Private Interface

@interface MapWithCustomAnnotationsViewController ()

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapWithCustomAnnotationsViewController Class Implementation

@implementation MapWithCustomAnnotationsViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Center map around the building
    
    SPQuery *buildingQuery = SPQuery.new;
    
    // Get list of building for current API Key
    
    [[SPMetaService sharedInstance] getBuildings:buildingQuery completion:^(NSArray<SPGeoJson *> * _Nullable buildings, NSError * _Nullable err) {
        if (buildings) {
            SPGeoJson *firstBuilding = buildings.firstObject;
            
            if (firstBuilding) {
                SPBoundingBox *boundingBox = [firstBuilding boundingBox];
                MGLCoordinateBounds mglBoundingBox = MGLCoordinateBoundsMake([boundingBox southWest], [boundingBox northEast]);
                UIEdgeInsets edgeInsets = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
                
                [[self mapView] setVisibleCoordinateBounds:mglBoundingBox edgePadding:edgeInsets animated:NO];
            }
        }
    }];
    
    [self addAllToilets];
    [self addKitchen];
}

#pragma mark - Example adding all toilets

-(void)addAllToilets {
    
    //First we remove all the old annotations we've placed onto the map
    NSMutableArray* annotationsToRemove = [NSMutableArray array];
    for (id<MGLAnnotation> annotation in self.mapView.annotations) {
        if ([annotation isKindOfClass: [CustomPointAnnotation class]]) {
            [annotationsToRemove addObject: annotation];
        }
    }
    [self.mapView removeAnnotations: annotationsToRemove];
    
    //Fetch toilet points and add them to the map on the correct floors
    SPQuery* query = [SPQuery new];
    query.tags = @[@"Toilet", @"service_toilet"];
    
    [[SPMetaService sharedInstance] getPointsOfInterest: query completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        /**
            Add annotations for each point of interest that has
            an annotation and a floor associated.
        */
        if (points.count > 0) {
            for (SPGeoJson* point in points) {
                if (point.annotation && point.floor) {
                    NSString* name = point.properties[@"localRef"];
                    CustomPointAnnotation* customPointAnnotation = [CustomPointAnnotation new];
                    customPointAnnotation.coordinate = point.coordinate;
                    customPointAnnotation.text = name;
                    
                    [self.mapView addAnnotation: customPointAnnotation floor: point.floor];
                }
            }
        }
    }];
}

#pragma mark - Example adding a POI with a unique tag

-(void)addKitchen {
    
    //First we remove all the old annotations we've placed onto the map
    NSMutableArray* annotationsToRemove = [NSMutableArray array];
    for (id<MGLAnnotation> annotation in self.mapView.annotations) {
        if ([annotation isKindOfClass: [CustomPointAnnotation class]]) {
            [annotationsToRemove addObject: annotation];
        }
    }
    [self.mapView removeAnnotations: annotationsToRemove];
    
    //Fetch toilet points and add them to the map on the correct floors
    SPQuery* query = [SPQuery new];
    query.tags = @[@"Kitchen"];
    
    [[SPMetaService sharedInstance] getPointsOfInterest: query completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        /**
         Add annotations for each point of interest that has
         an annotation and a floor associated.
         */
        if (points.count > 0) {
            for (SPGeoJson* point in points) {
                if (point.annotation && point.floor) {
                    NSString* name = point.properties[@"localRef"];
                    CustomPointAnnotation* customPointAnnotation = [CustomPointAnnotation new];
                    customPointAnnotation.coordinate = point.coordinate;
                    customPointAnnotation.text = name;
                    
                    [self.mapView addAnnotation: customPointAnnotation floor: point.floor];
                }
            }
        }
    }];
}

#pragma mark - SPMapViewDelegate

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    //Check annotation type
    if ([annotation isKindOfClass: [CustomPointAnnotation class]]) {
        CustomPointAnnotation* customPointAnnotation = (CustomPointAnnotation*)annotation;
        
        NSString *reuseIdentifier = @"customReuseIdentifier";
        CustomAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        if (!annotationView) {
            annotationView = [[CustomAnnotationView alloc] initWithReuseIdentifier:reuseIdentifier];
            __weak typeof(self) wself = self;
            annotationView.onClickBlock = ^(NSString* text, CustomPointAnnotation* clickedAnnotation) {
                __strong typeof(wself) sself = wself;
                if (sself) {
                    //Handle click event
                    NSLog(@"Clicked button with text: %@", text);
                }
            };
        }
        
        //Set info for annotation view
        [annotationView setText: customPointAnnotation.text];
        return annotationView;
    } else {
        SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
        annotationView.annotationColor = [UIColor redColor];    //set annotation color
        return annotationView;
    }
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

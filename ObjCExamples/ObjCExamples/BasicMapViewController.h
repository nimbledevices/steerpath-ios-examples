//
//  BasicMapViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - BasicMapViewController Class Definition

/**
    This class is the most basic map implementation without
    any bluetooth or gps positioning.
*/
@interface BasicMapViewController : UIViewController

@end

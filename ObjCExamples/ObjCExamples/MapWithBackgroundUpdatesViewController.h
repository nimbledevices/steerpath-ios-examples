//
//  MapWithBackgroundUpdatesViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 03/09/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
    MapWithBackgroundUpdatesViewController
 
    This class implements showing a map with your location as
    well as providing an example on how to implement a location manager that
    works while the application is running in background mode.
 */
@interface MapWithBackgroundUpdatesViewController : UIViewController

@end

NS_ASSUME_NONNULL_END

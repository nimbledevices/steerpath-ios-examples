//
//  MapAnnotationsWithPopupViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapAnnotationsWithPopupViewController Class Definition

/**
    This class implements annotations that will open a customizable popup view
    when clicked.
*/
@interface MapAnnotationsWithPopupViewController : UIViewController

@end

//
//  MapWithAnnotationsViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithAnnotationsViewController.h"
@import Steerpath;

#pragma mark - MapWithAnnotationsViewController Private Interface

@interface MapWithAnnotationsViewController ()<SPMapViewDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapWithAnnotationsViewController Class Implementation

@implementation MapWithAnnotationsViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.169009, 24.941390);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 13.0f animated: NO];
    
    /**
        Add annotations to map. These annotations will be visible all the time
        until they are removed from the map. via 'removeAnnotation:' method.
    */
    MGLPointAnnotation* pointAnnotation = [MGLPointAnnotation new];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(60.168231, 24.935989);
    
    MGLPointAnnotation* pointAnnotation2 = [MGLPointAnnotation new];
    pointAnnotation2.coordinate = CLLocationCoordinate2DMake(60.174605, 24.945469);
    
    MGLPointAnnotation* pointAnnotation3 = [MGLPointAnnotation new];
    pointAnnotation3.coordinate = CLLocationCoordinate2DMake(60.167404, 24.943482);
    
    [self.mapView addAnnotation: pointAnnotation];
    [self.mapView addAnnotation: pointAnnotation2];
    [self.mapView addAnnotation: pointAnnotation3];
}

#pragma mark - SPMapViewDelegate

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    /**
        This will dequeue an annotation view provided by the Steerpath SDK
    */
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    annotationView.annotationColor = [UIColor redColor];    //set annotation color
    return annotationView;
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

//
//  MapWithBackgroundUpdatesViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 03/09/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies
@import Steerpath;
#import "MapWithBackgroundUpdatesViewController.h"
#import "SteerpathBackgroundLocationManager.h"

#pragma mark - MapWithBackgroundUpdatesViewController Private Interface

@interface MapWithBackgroundUpdatesViewController ()<SPMapViewDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapWithBackgroundUpdatesViewController Class Implementation

@implementation MapWithBackgroundUpdatesViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Start the background location manager
    [[SteerpathBackgroundLocationManager sharedInstance] start];
    
    /**
        In this example we listen to notifications that are sent from the 'SteerpathBackgroundLocationManager'.
        SteerpathBackgroundLocationManager will stay alive even when this view controller is not active and will provide a bluetooth position
        when the application is running in the background.
    */
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(locationNotification:) name: kSPLocationUpdatedNotification object: nil];
}

#pragma mark - Notifications

-(void)locationNotification:(NSNotification*)notification {
    if ([notification.name isEqualToString: kSPLocationUpdatedNotification]) {
        [self.mapView setCurrentLocation: notification.object];
    }
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

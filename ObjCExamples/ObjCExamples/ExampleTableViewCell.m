//
//  ExampleTableViewCell.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "ExampleTableViewCell.h"

#pragma mark - Constants

NSString* const kExampleTableViewCellIdentifier = @"ExampleTableViewCell";
NSString* const kExampleTableViewCellNib = @"ExampleTableViewCell";

#pragma mark - ExampleTableViewCell Class Implementation

@implementation ExampleTableViewCell

@end

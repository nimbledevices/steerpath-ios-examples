//
//  MapWithFloorAnnotationsViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithFloorAnnotationsViewController Class Definition

/**
    This class implements a map with annotations that are visible on specific floors.
*/
@interface MapWithFloorAnnotationsViewController : UIViewController

@end

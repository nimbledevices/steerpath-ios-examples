//
//  MapWithDebugPositionViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 01/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithDebugPositionViewController Class Definition

/**
    This class implements a map and uses a debug location
    for positioning.
*/
@interface MapWithDebugPositionViewController : UIViewController

@end

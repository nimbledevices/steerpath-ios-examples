//
//  MapWithAnnotationsViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithAnnotationsViewController Class Definition

/**
    This class implements a map and floor specific annotations views.
*/
@interface MapWithAnnotationsViewController : UIViewController

@end

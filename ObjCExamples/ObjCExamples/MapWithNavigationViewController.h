//
//  MapWithNavigationViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 20/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithNavigationViewController Class Definition

/**
    This class implements a map, indoor positioning and an example how you
    would implement navigation for an app.
*/
@interface MapWithNavigationViewController : UIViewController

@end

//
//  MapWithRouteViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 20/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithRouteViewController Class Definition

/**
    This class implements a map, indoor positioning and drawing a static route on the map.
    Static route in this case refers to a route that does not change depending on users location.
*/
@interface MapWithRouteViewController : UIViewController

@end

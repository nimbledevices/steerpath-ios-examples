//
//  SteerpathBackgroundLocationManager.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 03/09/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Constants

extern NSString* const kSPLocationUpdatedNotification;

#pragma mark - SteerpathBackgroundLocationManager Class Definition

/**
    SteerpathBackgroundLocationManager
 
    This is a helper class for creating a location manager that can run
    even while the application is running in background mode.
 
    To receive background updates, make sure you your app has 'Background Modes' capabilities with the 'Uses Bluetooth LE accessories' enabled.
*/
@interface SteerpathBackgroundLocationManager : NSObject

#pragma mark - Object Lifecycle

/**
    @return Singleton instance of SteerpathBackgroundLocationManager
*/
+(instancetype)sharedInstance;

/** unavailable methods, do not use these */
-(instancetype) init __attribute__((unavailable("call sharedInstance instead")));

/**
    Starts location updates
*/
-(void)start;

/**
    Stops location updates
*/
-(void)stop;

@end

NS_ASSUME_NONNULL_END

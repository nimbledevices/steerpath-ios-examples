//
//  Examples.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <Foundation/Foundation.h>

#pragma mark - Enums

typedef NS_ENUM(NSUInteger, ExampleType) {
    ExampleTypeBasicMap,
    ExampleTypeMapWithPositioning,
    ExampleTypeMapWithDebugPosition,
    ExampleTypeMapWithRoute,
    ExampleTypeMapWithNavigation,
    ExampleTypeMapWithAnnotations,
    ExampleTypeMapWithCustomAnnotations,
    ExampleTypeMapWithFloorAnnotations,
    ExampleTypeAnnotationsFromMeta,
    ExampleTypeAnnotationsWithPopup,
    ExampleTypeFeatureSelection,
    ExampleTypeLiveUsers,
    ExampleTypeBackgroundUpdates,
    ExampleTypeCount         //number of example types
};

static NSString* EXAMPLE_NAME(ExampleType type) {
    NSString* name = @"Unknown";
    switch (type) {
        case ExampleTypeBasicMap:
            name = @"Map";
            break;
        case ExampleTypeMapWithPositioning:
            name = @"Positioning";
            break;
        case ExampleTypeMapWithDebugPosition:
            name = @"Debug position";
            break;
        case ExampleTypeMapWithRoute:
            name = @"Static route";
            break;
        case ExampleTypeMapWithNavigation:
            name = @"Navigation";
            break;
        case ExampleTypeMapWithAnnotations:
            name = @"Annotations";
            break;
        case ExampleTypeMapWithCustomAnnotations:
            name = @"Custom annotations";
            break;
        case ExampleTypeMapWithFloorAnnotations:
            name = @"Floor specific annotations";
            break;
        case ExampleTypeAnnotationsFromMeta:
            name = @"Annotations from Meta Service";
            break;
        case ExampleTypeAnnotationsWithPopup:
            name = @"Annotations with popups";
            break;
        case ExampleTypeFeatureSelection:
            name = @"Feature selection";
            break;
        case ExampleTypeLiveUsers:
            name = @"Live Users";
            break;
        case ExampleTypeBackgroundUpdates:
            name = @"Background updates";
            break;
        default:
            break;
    }
    return name;
}

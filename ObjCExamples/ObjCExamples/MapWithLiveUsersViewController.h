//
//  MapWithLiveUsersViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 29/05/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithLiveUsersViewController Class Definition

/**
    MapWithLiveUsersViewController
 
    This class implements showing a map with your location as
    well as showing other users on the map via Steerpath Live APIs
*/
@interface MapWithLiveUsersViewController : UIViewController

@end

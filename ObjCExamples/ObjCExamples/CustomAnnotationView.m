//
//  CustomAnnotationView.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 22/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "CustomAnnotationView.h"

#pragma mark - CustomAnnotationView Private Interface

@interface CustomAnnotationView()

@property (nonatomic, strong) UIButton* button;
@property (nonatomic, strong) UILabel* textLabel;

@property (nonatomic, strong) NSString* currentText;

@end

#pragma mark - CustomAnnotationView Class Implementation

@implementation CustomAnnotationView

#pragma mark - Object Lifecycle

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier: reuseIdentifier];
    if (self) {
        /**
            Here you would add custom subviews, load nibs etc.
            and add them as a subview.
         
            Currently Mapbox does not have an API for loading reusable annotation views
            straight from xib files.
        */
        UIView* customSubView = [[[NSBundle mainBundle] loadNibNamed:@"CustomAnnotationView" owner: self options:nil] objectAtIndex:0];
        [self addSubview: customSubView];
        
        self.button = (UIButton*)[customSubView viewWithTag: 450];
        [self.button addTarget: self action: @selector(buttonClicked:) forControlEvents: UIControlEventTouchUpInside];
        self.button.clipsToBounds = YES;
        self.textLabel = (UILabel*)[customSubView viewWithTag: 451];
        
        //Set frame to whatever the size of the custom subviews are
        self.frame = CGRectMake(0, 0, customSubView.frame.size.width, customSubView.frame.size.height);
        self.scalesWithViewingDistance = NO;
        self.enabled = YES;
        
        self.clipsToBounds = YES;
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Annotation view constraints
        NSLayoutConstraint* annotationWidthConstraint = [NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeWidth relatedBy: NSLayoutRelationGreaterThanOrEqual toItem: customSubView attribute: NSLayoutAttributeWidth multiplier: 1.0f constant: 0];
        NSLayoutConstraint* annotationHeightConstraint = [NSLayoutConstraint constraintWithItem: self attribute: NSLayoutAttributeHeight relatedBy: NSLayoutRelationGreaterThanOrEqual toItem: customSubView attribute: NSLayoutAttributeHeight multiplier: 1.0f constant: 0];
        NSArray* constraints = @[annotationWidthConstraint, annotationHeightConstraint];
        [NSLayoutConstraint activateConstraints: constraints];
    }
    return self;
}

#pragma mark - Public Methods

-(void)setText:(NSString*)text {
    self.currentText = text;
    self.textLabel.text = text;
}

#pragma mark - IBActions

-(IBAction)buttonClicked:(id)sender {
    NSLog(@"Button was clicked!");
    if (self.onClickBlock) {
        self.onClickBlock(self.currentText, (CustomPointAnnotation*)self.annotation);
    }
}

@end

//
//  MapWithCustomAnnotationsViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 23/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithCustomAnnotationsViewController Class Definition

/**
    This class implements a map with custom annotation views.
*/
@interface MapWithCustomAnnotationsViewController : UIViewController

@end

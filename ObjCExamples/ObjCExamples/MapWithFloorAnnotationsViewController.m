//
//  MapWithFloorAnnotationsViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithFloorAnnotationsViewController.h"
@import Steerpath;

#pragma mark - MapWithFloorAnnotationsViewController Private Interface

@interface MapWithFloorAnnotationsViewController ()

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapWithFloorAnnotationsViewController Class Implementation

@implementation MapWithFloorAnnotationsViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Center map around the building
    
    SPQuery *buildingQuery = SPQuery.new;
    
    // Get list of building for current API Key
    
    [[SPMetaService sharedInstance] getBuildings:buildingQuery completion:^(NSArray<SPGeoJson *> * _Nullable buildings, NSError * _Nullable err) {
        if (buildings) {
            SPGeoJson *firstBuilding = buildings.firstObject;
            
            if (firstBuilding) {
                SPBoundingBox *boundingBox = [firstBuilding boundingBox];
                MGLCoordinateBounds mglBoundingBox = MGLCoordinateBoundsMake([boundingBox southWest], [boundingBox northEast]);
                UIEdgeInsets edgeInsets = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
                
                [[self mapView] setVisibleCoordinateBounds:mglBoundingBox edgePadding:edgeInsets animated:NO];
            }
        }
    }];
    
    SPQuery* toiletQuery = SPQuery.new;
    toiletQuery.tags = @[@"Toilet", @"service_toilet"];
    
    /**
     Get list of toilet inside the building for current API Key
     */
    
    [SPMetaService.sharedInstance getPointsOfInterest:toiletQuery completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        if(points) {
            
            /**
             Then create point annotation with each returned value and add to mapView
             */
            
            for (SPGeoJson* point in points) {
                MGLPointAnnotation* annotation = MGLPointAnnotation.new;
                annotation.coordinate = point.coordinate;
                NSNumber* floor = [[NSNumber alloc] initWithInt:2];
                [self.mapView addAnnotation:annotation floor:floor];
            }
        }
    }];
    
}

#pragma mark - SPMapViewDelegate

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    /**
        This will dequeue an annotation view provided by the Steerpath SDK
    */
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    annotationView.annotationColor = [UIColor redColor];    //set annotation color
    return annotationView;
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

//
//  BasicMapViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "BasicMapViewController.h"
@import Steerpath;

#pragma mark - BasicMapViewController Private Interface

@interface BasicMapViewController ()

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - BasicMapViewController Class Implementation

@implementation BasicMapViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    [self.mapView setCenterCoordinate: CLLocationCoordinate2DMake(60.220946, 24.812392) zoomLevel: 20.0 animated: NO];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

//
//  CustomAnnotationView.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 22/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <Mapbox/Mapbox.h>

@class CustomPointAnnotation;

/** Block type for actions */
typedef void(^AnnotationClickBlock)(NSString* text, CustomPointAnnotation* annotation);

#pragma mark - CustomAnnotationView Class Definition

@interface CustomAnnotationView : MGLAnnotationView

@property (nonatomic, copy) AnnotationClickBlock onClickBlock;

-(void)setText:(NSString*)text;

@end

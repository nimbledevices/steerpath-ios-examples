//
//  MapWithLiveUsersViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 29/05/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithLiveUsersViewController.h"
@import Steerpath;

#pragma mark - MapWithLiveUsersViewController Private Interface

@interface MapWithLiveUsersViewController ()<SPLocationManagerDelegate, SPMapViewDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - MapWithLiveUsersViewController Class Implementation

@implementation MapWithLiveUsersViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Hides search bar
    SPMapViewConfig* mapConfig = [SPMapViewConfig new];
    mapConfig.showsSearch = NO;
    [self.mapView setConfig: mapConfig];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
     */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 14.0f animated: NO];
    
    //Set your live access token
    [[SPAccountManager sharedInstance] setLiveAccessToken: @"Enter-your-live-api-key-here"];
    
    /**
     Configure the app to send location updates to Steerpath Live
    */
    SPLiveUserConfig* liveConfig = [SPLiveUserConfig new];
    liveConfig.userIdentifier = @"example-user1";       //Each user should have a unique user identifier
    liveConfig.password = @"example-password1";         //Select a password for the user
    liveConfig.userGroups = @[@"employees"];            //Set which groups the user belongs to
    
    //Configure geofences for user
    liveConfig.neutralGeofences = @[@"Storage Room", @"Lobby"];
    liveConfig.allowedGeofences = @[@"Office"];
    liveConfig.forbiddenGeofences = @[@"Dangerzone"];
    
    [[SPLive sharedInstance] shareLocationWithConfig: liveConfig completion:^(NSError * _Nullable error, BOOL started) {
        if (!error && started) {
            NSLog(@"Started SPLive!");
        } else {
            NSLog(@"Failed to start SPLive!");
        }
    }];
    
    //Configure the map to show live users for the group 'employees'
    SPLiveMapConfig* liveMapConfig = [SPLiveMapConfig new];
    liveMapConfig.groups = @[@"employees", @"managers", @"drivers"];                             //Set which groups should be visible on the map
    [self.mapView showLiveUsersWithConfig: liveMapConfig];
    
    /**
        Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
     
        When users position changes, a callback will be made to the delegate method
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
     */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    /**
        Pass the location object to the map to show users location.
    */
    [self.mapView setCurrentLocation: location];
}

#pragma mark - SPMapViewDelegate

-(NSString *)mapView:(SPMapView *)mapView titleForLiveUser:(NSString *)userIdentifier {
    //Set title for the user based on user identifier. For this example we only have one user called 'example-user1'
    if ([userIdentifier isEqualToString: @"example-user1"]) {
        return @"Test User";
    } else {
        return @"Unknown Person";
    }
}

-(void)mapView:(SPMapView *)mapView didUpdateLiveUsers:(NSArray<SPLiveUserInfo *> *)users {
    /**
     Example: Sort users per geofence
     ================================
     - In this example we will sort users into a dictionary that has an array of SPLiveUserInfo objects for each geofence identifier.
     - Once sorted into arrays, we print out the results.
     - Here's an example of the resulting dictionary structure:
     {
     "geofence-identifier1": [userInfo1, userInfo3],
     "geofence-identifier2": [userInfo2],
     "geofence-identifier3": [userInfo1, userInfo4, userInfo5]
     }
     */
    NSMutableDictionary* geofenceDictionary = [NSMutableDictionary new];
    for (SPLiveUserInfo* userInfo in users) {
        for (NSString* geofenceId in userInfo.geofencesHit) {
            NSMutableArray* geofenceArray = geofenceDictionary[geofenceId];
            if ([geofenceArray isKindOfClass: [NSMutableArray class]]) {
                [geofenceArray addObject: userInfo];
            } else {
                geofenceArray = [NSMutableArray array];
                [geofenceArray addObject: userInfo];
                geofenceDictionary[geofenceId] = geofenceArray;
            }
        }
    }
    //Print out the dictionary containing geofences and users
    NSLog(@"Users sorted into geofences:\n%@", geofenceDictionary);
    
    /**
     Example: Checking geofence status for each user
     ===============================================
     - In this example we check each users geofence status and print a text based on the result.
     */
    for (SPLiveUserInfo* userInfo in users) {
        if (userInfo.geofenceStatus == SPGeofenceStatusUnknown) {
            NSLog(@"%@ is outside of any of the users geofences", userInfo.userIdentifier);
        } else if (userInfo.geofenceStatus == SPGeofenceStatusNeutral) {
            NSLog(@"%@ is inside a neutral geofence", userInfo.userIdentifier);
        } else if (userInfo.geofenceStatus == SPGeofenceStatusAllowed) {
            NSLog(@"%@ is inside an allowed geofence", userInfo.userIdentifier);
        }  else if (userInfo.geofenceStatus == SPGeofenceStatusForbidden) {
            NSLog(@"%@ is inside a forbidden geofence", userInfo.userIdentifier);
        }
    }
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

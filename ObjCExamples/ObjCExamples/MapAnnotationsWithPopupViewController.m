//
//  MapAnnotationsWithPopupViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapAnnotationsWithPopupViewController.h"
#import "CustomCalloutView.h"
@import Steerpath;

#pragma mark - MapAnnotationsWithPopupViewController Private Interface

@interface MapAnnotationsWithPopupViewController ()

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapAnnotationsWithPopupViewController Class Implementation

@implementation MapAnnotationsWithPopupViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.169009, 24.941390);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 13.0f animated: NO];
    
    CustomPointAnnotation* pointAnnotation = [CustomPointAnnotation new];
    pointAnnotation.coordinate = CLLocationCoordinate2DMake(60.168231, 24.935989);
    pointAnnotation.title = @"First annotation";
    
    CustomPointAnnotation* pointAnnotation2 = [CustomPointAnnotation new];
    pointAnnotation2.coordinate = CLLocationCoordinate2DMake(60.174605, 24.945469);
    pointAnnotation2.title = @"Second annotation";
    
    [self.mapView addAnnotation: pointAnnotation];
    [self.mapView addAnnotation: pointAnnotation2];
}

#pragma mark - SPMapViewDelegate

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    /**
        This will dequeue an annotation view provided by the Steerpath SDK
    */
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    annotationView.annotationColor = [UIColor redColor];    //set annotation color
    return annotationView;
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

-(BOOL)mapView:(MGLMapView *)mapView annotationCanShowCallout:(id <MGLAnnotation>)annotation {
    /**
        Allow callout view to be shown for custom point annotations
    */
    return [annotation isKindOfClass: [CustomPointAnnotation class]];
}

-(UIView<MGLCalloutView>*)mapView:(MGLMapView *)mapView calloutViewForAnnotation:(id <MGLAnnotation>)annotation {
    
    //Check that we're asking a callout view for a custom point annotation
    if ([annotation isKindOfClass: [CustomPointAnnotation class]]) {
        CustomCalloutView* calloutView = [CustomCalloutView new];
        calloutView.representedObject = annotation;
        calloutView.anchoredToAnnotation = YES;
        calloutView.dismissesAutomatically = NO;
        
        return calloutView;
    }
    
    return nil;
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

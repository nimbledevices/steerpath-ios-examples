//
//  MapWithPositioningViewController.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - MapWithPositioningViewController Class Definition

/**
    This class implements a map along with basic bluetooth positioning.
*/
@interface MapWithPositioningViewController : UIViewController

@end

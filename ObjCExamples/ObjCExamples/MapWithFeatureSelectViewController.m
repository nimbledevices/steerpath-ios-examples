//
//  MapWithFeatureSelectViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 08/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithFeatureSelectViewController.h"
#import "CustomAnnotationView.h"
#import "CustomCalloutView.h"
@import Steerpath;

#pragma mark - MapWithFeatureSelectViewController Private Interface

@interface MapWithFeatureSelectViewController ()<SPMapViewDelegate, SPLocationManagerDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

//Contains selected map feature localRef and annotation
@property (nonatomic, strong) NSDictionary<NSString*,id<MGLAnnotation>>* selectedFeature;

@end

#pragma mark - MapWithFeatureSelectViewController Class Implementation

@implementation MapWithFeatureSelectViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    [self.mapView setCenterCoordinate: CLLocationCoordinate2DMake(60.185893, 24.827614) zoomLevel: 14.5 animated: NO];
    
    /**
     Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
     If bluetooth is not on, the app will ask the user to turn on bluetooth.
     Also asks permission to use iOS Location Services (GPS).
     
     When users position changes, a callback will be made to the delegate method
     -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
     */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
}

#pragma mark - Get Point Of Interest for 'localRef'

/**
    Example on how you would fetch a point of interest from SPMetaService
    for a 'localRef' assuming the 'localRef' is a tag.
*/
-(void)addAnnotationFor:(NSString*)localRef {
    //Query with 'localRef' as tag
    SPQuery* query = [SPQuery new];
    query.tags = @[localRef];
    
    /**
        Requesting point of interest data from Steerpath Meta Service. Requires internet
        connection when first time downloading a particular set of data. Will cache data locally.
     
        To use the SPMetaService you must have a Steerpath API key.
        See SPAccountManager.h for more information.
     */
    [[SPMetaService sharedInstance] getPointsOfInterest: query completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        /**
            Add annotation onto map if a point of interest was found with the
            tag.
        */
        if (points.count > 0) {
            SPGeoJson* first = points.firstObject;
            if (first.floor && CLLocationCoordinate2DIsValid(first.coordinate)) {
                if (first.annotation && first.floor) {
                    NSString* name = first.properties[@"localRef"];
                    CustomPointAnnotation* customPointAnnotation = [CustomPointAnnotation new];
                    customPointAnnotation.coordinate = first.coordinate;
                    customPointAnnotation.text = name;
                    customPointAnnotation.location = first.location;
                    
                    [self.mapView addAnnotation: customPointAnnotation floor: first.floor];
                }
            }
        }
    }];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    
    /**
        Pass the location object to the map to show users location.
    */
    [self.mapView setCurrentLocation: location];
}

#pragma mark - SPMapViewDelegate

-(void)mapView:(SPMapView *)mapView didSelectFeatures:(NSArray<id<MGLFeature>> *)features {
    for (id<MGLFeature> feature in features) {
        NSString* localRef = nil;
        if ([feature isKindOfClass: [MGLPointFeature class]]) {
            NSDictionary* attributes = ((MGLPointFeature*)feature).attributes;
            localRef = attributes[@"localRef"];
        } else if ([feature isKindOfClass: [MGLPolygonFeature class]]) {
            NSDictionary* attributes = ((MGLPolygonFeature*)feature).attributes;
            localRef = attributes[@"localRef"];
        }
        
        if ([localRef isKindOfClass: [NSString class]]) {
            [self addAnnotationFor: localRef];
            break;
        }
    }
}

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    annotationView.annotationColor = [UIColor redColor];    //set annotation color
    return annotationView;
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

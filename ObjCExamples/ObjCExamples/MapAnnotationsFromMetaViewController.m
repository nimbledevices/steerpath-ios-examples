//
//  MapAnnotationsFromMetaViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 21/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapAnnotationsFromMetaViewController.h"
@import Steerpath;

#pragma mark - MapAnnotationsFromMetaViewController Private Interface

@interface MapAnnotationsFromMetaViewController ()

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

@end

#pragma mark - MapAnnotationsFromMetaViewController Class Implementation

@implementation MapAnnotationsFromMetaViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Center map around the building
    
    SPQuery *buildingQuery = SPQuery.new;
    
    // Get list of building for current API Key
    
    [[SPMetaService sharedInstance] getBuildings:buildingQuery completion:^(NSArray<SPGeoJson *> * _Nullable buildings, NSError * _Nullable err) {
        if (buildings) {
            SPGeoJson *firstBuilding = buildings.firstObject;
            
            if (firstBuilding) {
                SPBoundingBox *boundingBox = [firstBuilding boundingBox];
                MGLCoordinateBounds mglBoundingBox = MGLCoordinateBoundsMake([boundingBox southWest], [boundingBox northEast]);
                UIEdgeInsets edgeInsets = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
                
                [[self mapView] setVisibleCoordinateBounds:mglBoundingBox edgePadding:edgeInsets animated:NO];
            }
        }
    }];
    
    /**
        Create a query object for fetching points of interest data.
     
        Added following parameters to query:
        - floor has to be equal to 2 (0 is ground floor)
        - tag "toilets"
    */
    SPQuery* query = [SPQuery new];
    query.floor = @(2);
    query.tags = @[@"Toilet", @"service_toilet"];
    
    /**
        Requesting point of interest data from Steerpath Meta Service. Requires internet
        connection when first time downloading a particular set of data. Will cache data locally.
     
        To use the SPMetaService you must have a Steerpath API key.
        See SPAccountManager.h for more information.
    */
    [[SPMetaService sharedInstance] getPointsOfInterest: query completion:^(NSArray<SPGeoJson *> * _Nullable points, NSError * _Nullable err) {
        /**
            Add annotations for each point of interest that has
            an annotation and a floor associated.
        */
        if (points.count > 0) {
            for (SPGeoJson* point in points) {
                if (point.annotation && point.floor) {
                    [self.mapView addAnnotation: point.annotation floor: point.floor];
                }
            }
        }
    }];
}

#pragma mark - SPMapViewDelegate

-(MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id<MGLAnnotation>)annotation {
    /**
        This will dequeue an annotation view provided by the Steerpath SDK
    */
    SPAnnotationView* annotationView = [self.mapView dequeueAnnotationViewForAnnotation: annotation];
    annotationView.annotationColor = [UIColor redColor];    //set annotation color
    return annotationView;
}

/** This is called when user selects an annotation */
-(void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id<MGLAnnotation>)annotation {
    /** Animates map to annotation coordinate */
    [self.mapView setCenterCoordinate: annotation.coordinate animated: YES];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

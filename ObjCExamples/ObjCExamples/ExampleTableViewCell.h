//
//  ExampleTableViewCell.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 17/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>

#pragma mark - Constants

extern NSString* const kExampleTableViewCellIdentifier;
extern NSString* const kExampleTableViewCellNib;

#pragma mark - ExampleTableViewCell Class Definition

@interface ExampleTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

@end

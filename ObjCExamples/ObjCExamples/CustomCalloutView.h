//
//  CustomCalloutView.h
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 22/02/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import <UIKit/UIKit.h>
@import Steerpath;

#pragma mark - CustomPointAnnotation Class Definition

/**
    This is a subclass of MGLPointAnnotation.
    Declared so we can distinguish between different types of annotations.
 */
@interface CustomPointAnnotation : MGLPointAnnotation

@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) SPLocation* location;

@end

#pragma mark - CustomCalloutView Class Definition

/**
    This is an example on how you can create a basic custom callout
    view with a text label.
*/
@interface CustomCalloutView : UIView<MGLCalloutView>

/**
    These are defined in the MGLCalloutView protocol
*/

@property (nonatomic, strong) id <MGLAnnotation> representedObject;
@property (nonatomic, weak) id<MGLCalloutViewDelegate> delegate;

@property (nonatomic, assign, getter=isAnchoredToAnnotation) BOOL anchoredToAnnotation;
@property (nonatomic, assign) BOOL dismissesAutomatically;

@end

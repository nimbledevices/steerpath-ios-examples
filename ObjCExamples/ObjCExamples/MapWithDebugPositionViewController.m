//
//  MapWithDebugPositionViewController.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 01/03/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "MapWithDebugPositionViewController.h"
@import Steerpath;

#pragma mark - MapWithDebugPositionViewController Private Interface

@interface MapWithDebugPositionViewController ()<SPLocationManagerDelegate>

//Map
@property (nonatomic, weak) IBOutlet SPMapView* mapView;

//Positioning
@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - MapWithDebugPositionViewController Class Implementation

@implementation MapWithDebugPositionViewController

#pragma mark - Object Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
        Centers map onto specific coordinates and zoom level.
        Zoom level 0 is the whole world map.
    */
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949);
    [self.mapView setCenterCoordinate: centerCoordinate zoomLevel: 14.0f animated: NO];
    
    /**
        Map view will start to follow users location.
    */
    [self.mapView setUserTrackingMode: MGLUserTrackingModeFollow animated: YES];
    
    /**
        Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
        If bluetooth is not on, the app will ask the user to turn on bluetooth.
        Also asks permission to use iOS Location Services (GPS).
     
        When users position changes, a callback will be made to the delegate method
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
    */
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
    
    
    /**
        We use a debug position instead of actually using a bluetooth or GPS position.
        This is useful for demonstration purposes or for example testing navigation/routes.
     
        After the debug location is set, the following delegate method will always return it:
        -(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location
    */
    CLLocationCoordinate2D debugCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949);
    SPLocation* location = [[SPLocation alloc] initWithCoordinate: debugCoordinate floor: 8 horizontalAccuracy: 1 course: 0 source: SPLocationSourceBluetooth];
    /**
        If you want to limit the user blue dot to a specific floor in a building
        you need to also set a building identifier to the location. Otherwise the
        location will be visible on all floors when viewing indoor maps.
     
        The unique building identifier can be found by fetching buildings via SPMetaService method
        'getBuildings' and checking the SPGeoJson identifier for that particular building.
     
    */
    location.building = @"574ec89bbb87115f2fa0c75a";    //example of a unique identifier
    
    [self.locationManager setDebugLocation: location];
}

#pragma mark - SPLocationManagerDelegate

/** This is called every time users location changes */
-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    
    /**
        Pass the location object to the map to show users location.
    */
    [self.mapView setCurrentLocation: location];
}

#pragma mark - IBActions

-(IBAction)menuButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated: true];
}

@end

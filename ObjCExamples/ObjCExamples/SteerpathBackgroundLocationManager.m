//
//  SteerpathBackgroundLocationManager.m
//  ObjCExamples
//
//  Created by Jussi Laakkonen on 03/09/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

#pragma mark - Dependencies

#import "SteerpathBackgroundLocationManager.h"
@import Steerpath;

#pragma mark - Constants

NSString* const kSPLocationUpdatedNotification = @"kSPLocationUpdatedNotification";

#pragma mark - SteerpathBackgroundLocationManager Private Interface

@interface SteerpathBackgroundLocationManager()<SPLocationManagerDelegate>

@property (nonatomic, strong) SPLocationManager* locationManager;

@end

#pragma mark - SteerpathBackgroundLocationManager Class Implementation

@implementation SteerpathBackgroundLocationManager

#pragma mark - Class / Static Methods

+(instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[super alloc] privateInit];
        [sharedInstance setup];
    });
    return sharedInstance;
}

#pragma mark - Object Lifecycle

-(instancetype)privateInit {
    return [super init];
}

-(void)setup {
    //Create an instance of the regular SPLocationManager object
    self.locationManager = [[SPLocationManager alloc] initWithDelegate: self];
}

-(void)start {
    //Turn on background updates
    self.locationManager.useBackgroundUpdates = YES;
    
    //Optionally you can use the 'backgroundUpdateInterval' to change how often location is calculated while app is in the background
    //self.locationManager.backgroundUpdateInterval = 30;   //seconds
}

-(void)stop {
    [self.locationManager stop];
}

#pragma mark - SPLocationManagerDelegate

-(void)spLocationManager:(SPLocationManager *)manager didUpdateLocation:(SPLocation *)location {
    //Send a notification containing the new location. See the example 'MapWithBackgroundUpdatesViewController' on how to listen to background location updates.
    [[NSNotificationCenter defaultCenter] postNotificationName: kSPLocationUpdatedNotification object: location];
}

@end

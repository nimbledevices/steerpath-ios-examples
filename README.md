# Steerpath iOS SDK Examples

Copyright Steerpath Ltd. 2018. All rights reserved

# Folder Structure

"ObjCExamples" contains example Xcode project written in Objective-C.

"SwiftExamples" contains example Xcode project written in Swift.

### Checking the Objective-C examples is highly recommended.

# Requirements
* Have CocoaPods installed on your OS X (https://cocoapods.org/)
* Have the necessary API access tokens provided by Steerpath.
* Don't have API access? Contact: support@steerpath.com

# How to build examples
* Go to either example folder and do the following
```
pod repo update
pod install
```
* Open the generated .xcworkspace with Xcode
* Add your API access token for the following key in the application Info.plist
```
SPSteerpathAccessToken
```
* Build!

# Integrating to your app

See instructions at https://bitbucket.org/nimbledevices/steerpath-sdk-ios-podspec

# Contact

support@steerpath.com

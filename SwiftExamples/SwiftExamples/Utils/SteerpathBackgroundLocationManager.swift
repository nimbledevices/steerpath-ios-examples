//
//  SteerpathBackgroundLocationManager.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 19/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: -Dependencies

import Foundation
import Steerpath

// MARK: Constants

public let kSPLocationUpdatedNotification: String = "kSPLocationUpdatedNotification"

// MARK: SteerpathBackgroundLocationManager

class SteerpathBackgroundLocationManager: NSObject {
    
    private var locationManager: SPLocationManager?
    
    static var shareInstance = SteerpathBackgroundLocationManager()
    
    private override init() {
        
        super.init()
        
        locationManager = SPLocationManager(delegate: self)
    }
    
    public func start() {
        //Turn on background updates
        self.locationManager?.useBackgroundUpdates = true
        //Optionally you can use the 'backgroundUpdateInterval' to change how often location is calculated while app is in the background
        //self.locationManager.backgroundUpdateInterval = 30;   //seconds
    }
    
    public func stop() {
        self.locationManager?.stop()
    }
    
}

extension SteerpathBackgroundLocationManager: SPLocationManagerDelegate {
    
    //Send a notification containing the new location. See the example 'MapWithBackgroundUpdatesViewController' on how to listen to background location updates.
    
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kSPLocationUpdatedNotification), object: location)
    }
    
}

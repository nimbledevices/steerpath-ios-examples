//
//  NavigationViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 03/04/2017.
//  Copyright © 2017 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import Steerpath

//MARK: NavigationViewController Class Definition

class NavigationViewController: UIViewController, SPMapViewDelegate, SPLocationManagerDelegate {
    
    //MARK: - Properties
    
    fileprivate var locationManager : SPLocationManager!
    @IBOutlet fileprivate var mapView : SPMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**
            Setting initial coordinate, zoom level and rotation for map view
        */
        self.mapView.setCenter(CLLocationCoordinate2D(latitude: 60.220946, longitude: 24.812392), zoomLevel: 19.0, animated: false)
        
        /**
            Location manager to provide positioning.
            Automatically starts locating.
        */
        self.locationManager = SPLocationManager(delegate: self)
        /**
            For the sake of this example we're providing a fake location
            because navigation requires some location before it can start.
         
            WARNING!!!
         
            Only use 'setDebugLocation' when developing your app and you don't
            have access to the building/area where you would normally do testing.
            Using 'setDebugLocation' will prevent normal blue dot / location behaviour
            and should be used with caution.
        */
        let fakeLocation = SPLocation(coordinate: CLLocationCoordinate2D(latitude: 60.22089814, longitude: 24.81234131), floor: 2, horizontalAccuracy: 1, course: 0)
        fakeLocation.building = "67"
        self.locationManager.setDebugLocation(fakeLocation)
        self.mapView.setCurrentLocation(fakeLocation)
        
        /**
            Get metadata for a place that is tagged as 'Storage' that is on the 2nd floor
         */
        let query = SPQuery()
        query.floor = 2
        query.tags = ["Toilet"];
        SPMetaService.sharedInstance().getPointsOfInterest(query) { [weak self] (geojsonArray, error) in
            guard let response = geojsonArray else {
                return
            }
            if (response.count > 0) {
                /**
                    For the sake of this example we select the first element in
                    the response. Notice that there may be multiple items in the
                    response and you may not want to always select the first one.
                 */
                if let pointLocation = response.first?.location() {
                    self?.mapView.startNavigation(to: pointLocation)
                }
            }
        }
    }
    
    //MARK: SPMapViewDelegate
    
    func mapView(_ mapView: SPMapView, navigationFailedWithError error: Error) {
        print("Navigation failed with error: ", error)
    }
    
    func mapView(_ mapView: SPMapView, navigationDidCalculate route: SPRoute) {
        print("Calculated route!");
    }
    
    func mapView(_ mapView: SPMapView, didNavigateTo location: SPLocation) {
        print("Reached a location!");
    }
    
    func mapView(_ mapView: MGLMapView, lineWidthForPolylineAnnotation annotation: MGLPolyline) -> CGFloat {
        /**
            This defines the width of polylines added to the map.
            SPRoute uses polylines to draw routes.
        */
        return 5.0
    }
    
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        /**
            This defines the color of polylines added to the map.
            SPRoute uses polylines to draw routes.
        */
        return UIColor.blue
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        /**
            Using default Steerpath SDK provided SPAnnotationView markers.
            Alternatively you may use your own markers.
        */
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        annotationView?.annotationColor = .blue
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        /**
            Plays a small animation indicating that an annotation was clicked
        */
        self.mapView.highlightAnnotation(annotation)
    }
    
    //MARK: - SPLocationManagerDelegate
    
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        /**
            Shows location provided by SPLocationManager on the map
        */
        self.mapView.setCurrentLocation(location)
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

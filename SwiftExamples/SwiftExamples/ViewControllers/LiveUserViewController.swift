//
//  LiveUserViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: Dependencies

import Foundation
import Steerpath

// MARK: LiveUserViewController Implementation

class LiveUserViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    private var locationManager: SPLocationManager!
    
    // MARK: -Object lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide search bar
        
        let mapConfig = SPMapViewConfig()
        mapConfig.showsSearch = false
        
        self.mapView.setConfig(mapConfig)
        
        // Instantiate locationManager
        
        locationManager = SPLocationManager(delegate: self)
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
        // Set your live access token
        SPAccountManager.sharedInstance().setLiveAccessToken("Enter-your-live-api-key-here")
        
        /**
         Configure the app to send location updates to Steerpath Live
         */
        
        let liveUserConfig = SPLiveUserConfig()
        liveUserConfig.userIdentifier = "example-user1"       //Each user should have a unique user identifier
        liveUserConfig.password = "example-password1"        //Select a password for the user
        liveUserConfig.userGroups = ["employees"]            //Set which groups the user belongs to
        
        //Configure geofences for user
        
        liveUserConfig.neutralGeofences = ["Storage Room", "Lobby"]
        liveUserConfig.allowedGeofences = ["panicroom101"]
        liveUserConfig.forbiddenGeofences = ["dangerzone101"]
        
        SPLive.sharedInstance().shareLocation(with: liveUserConfig) { (error, started) in
            if(error == nil && started) {
                print("Started SPLive")
            } else {
                print("Failed to start SPLive")
            }
        }
        
        //Configure the map to show live users for the group 'employees'
        let liveMapConfig = SPLiveMapConfig()
        liveMapConfig.groups = ["employees"]       // Set which group should be visible
        
        self.mapView.showLiveUsers(with: liveMapConfig)
        
    }
    
    // MARK: IBAction
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

// MARK: SPMapViewDelegate

extension LiveUserViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        self.mapView.setCenter(annotation.coordinate, animated: true)
    }
    
    func mapView(_ mapView: SPMapView, titleForLiveUser userIdentifier: String) -> String? {

        //Set title for the user based on user identifier. For this example we only have one user called 'example-user1'

        if userIdentifier == "example-user1" {
            return "Test User"
        } else {
            return "Unknown person"
        }
    }
    
    func mapView(_ mapView: SPMapView, didUpdateLiveUsers users: [SPLiveUserInfo]) {
        /**
         Example: Sort users per geofence
         ================================
         - In this example we will sort users into a dictionary that has an array of SPLiveUserInfo objects for each geofence identifier.
         - Once sorted into arrays, we print out the results.
         - Here's an example of the resulting dictionary structure:
         {
         "geofence-identifier1": [userInfo1, userInfo3],
         "geofence-identifier2": [userInfo2],
         "geofence-identifier3": [userInfo1, userInfo4, userInfo5]
         }
         */
        for user in users {
            print(user.geofencesHit)
        }
        
        var dict = Dictionary<String, [SPLiveUserInfo]>()
        
        for userInfo in users {
            for geofenceId in userInfo.geofencesHit {
                var geoFenceArray = dict[geofenceId]
                if geoFenceArray == nil {
                    geoFenceArray = [SPLiveUserInfo]()
                    geoFenceArray?.append(userInfo)
                    dict[geofenceId] = geoFenceArray!
                } else {
                    geoFenceArray?.append(userInfo)
                }
            }
        }
        
        //Print out the dictionary containing geofences and users
        
        print(dict)
        
        /**
         Example: Checking geofence status for each user
         ===============================================
         - In this example we check each users geofence status and print a text based on the result.
         */
        
        for user in users {
            if user.geofenceStatus == SPGeofenceStatus.unknown {
                print("\(user.userIdentifier) is outside of any of the users geofences")
            } else if (user.geofenceStatus == SPGeofenceStatus.neutral) {
                print("\(user.userIdentifier) is inside a neutral geofence")
            } else if (user.geofenceStatus == SPGeofenceStatus.allowed) {
                print("\(user.userIdentifier) is inside an allowed geofence")
            } else if (user.geofenceStatus == SPGeofenceStatus.forbidden) {
                print("\(user.userIdentifier) is inside a forbidden geofence")
            }
        }
        
    }
}

// MARK: SPLocationManager

extension LiveUserViewController: SPLocationManagerDelegate {
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        self.mapView.setCurrentLocation(location)
    }
}

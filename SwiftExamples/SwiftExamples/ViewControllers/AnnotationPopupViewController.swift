//
//  AnnotationPopupViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: - Dependencies

import Foundation
import Steerpath

// MARK: -AnnotationPopupViewController implementation

class AnnotationPopupViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    // MARK: - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**
         Centers map onto specific coordinates and zoom level.
         Zoom level 0 is the whole world map.
         */
        let centerCoordinate = CLLocationCoordinate2DMake(60.169009, 24.941390)
        
        self.mapView.setCenter(centerCoordinate, zoomLevel: 13, animated: false)
        
        let pointAnnotation = CustomPointAnnotation()
        pointAnnotation.coordinate = CLLocationCoordinate2DMake(60.168231, 24.935989)
        pointAnnotation.title = "First annotation"
        
        let pointAnnotation2 = CustomPointAnnotation()
        pointAnnotation2.coordinate = CLLocationCoordinate2DMake(60.174605, 24.945469)
        pointAnnotation2.title = "Second annotation"
        
        self.mapView.addAnnotation(pointAnnotation)
        self.mapView.addAnnotation(pointAnnotation2)

    }
    
    // MARK: IBActions
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension AnnotationPopupViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        /**
         This will dequeue an annotation view provided by the Steerpath SDK
         */
        
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        
        annotationView?.annotationColor = .red
        
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return (annotation is CustomPointAnnotation)
    }
    
    /** This is called when user selects an annotation */
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        /** Animates map to annotation coordinate */
        self.mapView.setCenter(annotation.coordinate, animated: true)
    }
    
    func mapView(_ mapView: MGLMapView, calloutViewFor annotation: MGLAnnotation) -> MGLCalloutView? {
        if (annotation is CustomPointAnnotation) {
            let customCalloutView = CustomCalloutView(annotation: annotation as! CustomPointAnnotation)
            
            return customCalloutView
        }
        
        return nil
    }
}

//
//  GeoFenceViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 03/12/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: Dependencies

import Foundation
import Steerpath

// MARK: GeoFenceViewController Implementation

class GeoFenceViewController: UIViewController {
    
    // MARK: Properties
    
    private var locationManager: SPLocationManager!
    private var lounges: [SPGeoJson]?
    private var geoFenceIdentifier: String?
    
    // MARK: IBOutlets
    @IBOutlet weak var mapView: SPMapView!
    
    
    // MARK: -Object lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = SPLocationManager(delegate: self)
        
        centerMap()
        
        addMobileRoomToGeofence()
        
        // Register notification to observe with key: kSPGeofenceEventNotification to listen for geoFence event.
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleHitGeofences(_:)), name: NSNotification.Name.spGeofenceEvent, object: nil)
    }
    
    // MARK: IBActions
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: Utils
    private func addMobileRoomToGeofence() {
        let query = SPQuery()
        query.tags = ["Mobile development"]
        
        SPMetaService.sharedInstance().getPointsOfInterest(query) { (mobileRooms, error) in
            guard let mobileRooms = mobileRooms else { return }
            
            for mobileRoom in mobileRooms {
                guard let geofences = mobileRoom.geofences() else { continue }
                
                // Add geofences to SPGeofenceManager
                
                for geofence in geofences {
                    var error: NSErrorPointer
                    SPGeofenceManager.sharedInstance().add(geofence, error: error)
                    
                    // get and set geoFenceIdentifier for the mobile development room
                    
                    self.geoFenceIdentifier = geofence.identifier()
                }
                
            }
        }
    }
    
    private func centerMap() {
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
    }
    
    @objc private func handleHitGeofences(_ notification: NSNotification) {
        guard let event = notification.object as? SPGeofenceEvent else { return }
        
        
        for hitGeofence in event.geofences {
            guard let geoFenceIdentifier = self.geoFenceIdentifier else { break }
            // Check the geofence identifier if it is the same
            if hitGeofence.identifier() == geoFenceIdentifier {
                switch event.transition {
                case .enter:
                    print("User did enter mobile development room")
                    break
                case .exit:
                    print("User did exit mobile development room")
                    break
                default:
                    ()
                }
            }
        }
    }
    
}

// MARK: SPLocationManagerDelegate

extension GeoFenceViewController: SPLocationManagerDelegate {
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        self.mapView.setCurrentLocation(location)
        
        // Update location with SPGeofenceManager
        
        SPGeofenceManager.sharedInstance().update(with: location)
    }
}


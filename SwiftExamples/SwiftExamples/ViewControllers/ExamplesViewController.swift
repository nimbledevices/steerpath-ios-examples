//
//  ExamplesViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 20/02/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit

//MARK: - Enums

enum Example : Int {
    case map = 0
    case position = 1
    case navigation = 2
    case annotations = 3
    case debugLocation = 4
    case staticRoute = 5
    case customAnnotation = 6
    case floorSpecificAnnotations = 7
    case annotationMetaService = 8
    case annotationPopUp = 9
    case featureSelection = 10
    case liveUser = 11
    case backgroundUpdate = 12
    case geofence = 13
    case count = 14
    
}

//MARK: - ExamplesViewController Class Implementation

class ExamplesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var examplesTableView : UITableView!

    //MARK: - Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let exampleNib = UINib(nibName: ExampleTableViewCell.nibName, bundle: nil)
        examplesTableView.register(exampleNib, forCellReuseIdentifier: ExampleTableViewCell.identifier)
    }
    
    //MARK: - UITableView
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ExampleTableViewCell = tableView.dequeueReusableCell(withIdentifier: ExampleTableViewCell.identifier, for: indexPath) as! ExampleTableViewCell
        cell.titleLabel.text = ExamplesViewController.titleForExample(indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Example.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: ExamplesViewController.segueForExample(indexPath.row), sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    //MARK: - Helpers
    
    static func titleForExample(_ example : Int) -> String {
        var title = "Example"
        switch example {
        case Example.map.rawValue:
            title = "Map"
            break
        case Example.position.rawValue:
            title = "Position"
            break
        case Example.navigation.rawValue:
            title = "Navigation"
            break
        case Example.annotations.rawValue:
            title = "Annotations"
            break
        case Example.debugLocation.rawValue:
            title = "Debug Location"
            break
        case Example.staticRoute.rawValue:
            title = "Static Route"
            break
        case Example.customAnnotation.rawValue:
            title = "Custom Annotation"
            break
        case Example.floorSpecificAnnotations.rawValue:
            title = "Floor specific annotations"
            break
        case Example.annotationMetaService.rawValue:
            title = "Annotations from Meta service"
            break
        case Example.annotationPopUp.rawValue:
            title = "Annotation with popups"
            break
        case Example.featureSelection.rawValue:
            title = "Feature Selection"
            break
        case Example.liveUser.rawValue:
            title = "Live Users"
            break
        case Example.backgroundUpdate.rawValue:
            title = "Background updates"
        case Example.geofence.rawValue:
            title = "Geofence"
        default:
            title = "Example"
        }
        return title
    }
    
    static func segueForExample(_ example : Int) -> String {
        var segue = "ShowMap"
        switch example {
        case Example.map.rawValue:
            segue = "ShowMap"
            break
        case Example.position.rawValue:
            segue = "ShowPosition"
            break
        case Example.navigation.rawValue:
            segue = "ShowNavigation"
            break
        case Example.annotations.rawValue:
            segue = "ShowAnnotations"
            break
        case Example.debugLocation.rawValue:
            segue = "ShowDebugLocation"
            break
        case Example.staticRoute.rawValue:
            segue = "ShowStaticRoute"
            break
        case Example.customAnnotation.rawValue:
            segue = "ShowCustomAnnotaion"
            break
        case Example.floorSpecificAnnotations.rawValue:
            segue = "ShowFloorSpecificAnnotations"
            break
        case Example.annotationMetaService.rawValue:
            segue = "ShowAnnotationMetaService"
            break
        case Example.annotationPopUp.rawValue:
            segue = "ShowAnnotationPopup"
            break
        case Example.featureSelection.rawValue:
            segue = "ShowfeatureSelection"
            break
        case Example.liveUser.rawValue:
            segue = "ShowLiveUser"
            break
        case Example.backgroundUpdate.rawValue:
            segue = "ShowBackgroundUpdate"
            break
        case Example.geofence.rawValue:
            segue = "ShowGeoFence"
            break
        default:
            segue = "ShowMap"
        }
        return segue
    }
}

//
//  PositionViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 20/02/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import Steerpath

//MARK: - PositionViewController Class Implementation

class PositionViewController: UIViewController, SPMapViewDelegate, SPLocationManagerDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPMapView!
    @IBOutlet fileprivate var locationManager : SPLocationManager!
    
    //MARK: - Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /**
            Set the map center with a GPS coordinate and zoom level.
            The bigger the zoom level the more detailed the map is.
            Zoom level 0 = world map.
         */
        let coordinate = CLLocationCoordinate2DMake(60.220946, 24.812392)
        let zoom = 20.0
        self.mapView.setCenter(coordinate, zoomLevel: zoom, animated: false)
        
        /**
            Create a location manager. Location updates begin automatically
            and can be listened with the 'didUpdate' delegate method
         */
        self.locationManager = SPLocationManager(delegate: self)
    }
    
    //MARK: - SPLocationManagerDelegate
    
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        self.mapView.setCurrentLocation(location)
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

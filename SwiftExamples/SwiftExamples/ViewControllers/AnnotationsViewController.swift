//
//  AnnotationsViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 20/02/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import Steerpath

//MARK: - MapViewController Class Implementation

class AnnotationsViewController : UIViewController, SPMapViewDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPMapView!
    
    //MARK: - Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /**
            Set the map center with a GPS coordinate and zoom level.
            The bigger the zoom level the more detailed the map is.
            Zoom level 0 = world map.
         */
        let coordinate = CLLocationCoordinate2DMake(60.220946, 24.812392)
        let zoom = 20.0
        self.mapView.setCenter(coordinate, zoomLevel: zoom, animated: false)
        
        /**
            This adds annotations to every location that is tagged as a 'room'.
            This is an asynchronous request and will attempt to fetch matadata from the Steerpath servers.
        */
        let query = SPQuery()
        query.tags = ["room"]
        SPMetaService.sharedInstance().getPointsOfInterest(query) { [weak self] (geojsonArray, error) in
            guard let response = geojsonArray else {
                return
            }
            //Add a point annotation onto the map for each SPGeoJson object on the response
            for geojson in response {
                let pointAnnotation = MGLPointAnnotation()
                pointAnnotation.coordinate = geojson.coordinate()
                pointAnnotation.title = geojson.title()
                self?.mapView.addAnnotation( pointAnnotation, floor: geojson.floor())
            }
        }
    }
    
    //MARK: SPMapViewDelegate
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        /**
            This dequeues a standard SPAnnotationView and sets the color a blue
        */
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        annotationView?.annotationColor = .blue
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

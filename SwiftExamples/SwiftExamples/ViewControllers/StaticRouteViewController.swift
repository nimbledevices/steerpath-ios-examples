//
//  StaticRouteViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 15/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: Dependencies

import Foundation
import Steerpath

// MARK: StaticRouteViewController Implementation

class StaticRouteViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    private var locationManager: SPLocationManager!
    
    // MARK - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
        /**
         Map view will start to follow users location.
         */
        
        self.mapView.setUserTrackingMode(MGLUserTrackingMode.follow, animated: true)
        
        /**
         Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
         If bluetooth is not on, the app will ask the user to turn on bluetooth.
         Also asks permission to use iOS Location Services (GPS).
         
         When users position changes, a callback will be made to the delegate method
         spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation)
         */
        
        locationManager = SPLocationManager(delegate: self)
        
        /**
         Create a start location and an end location for a route.
         Note that locations can be on different floors.
         
         These will be used for calculating routes via SPDirectionsManager.
         */
        
        let startLocation = SPLocation(coordinate: CLLocationCoordinate2DMake(60.22094595, 24.81237337), floor: 2, horizontalAccuracy: 1, course: 0, source: .bluetooth)
        
        let endLocation = SPLocation(coordinate: CLLocationCoordinate2DMake(60.22098448, 24.81246399), floor: 2, horizontalAccuracy: 1, course: 0, source: .bluetooth)
        
        /**
         Create a directions request. Note that you can pass multiple destinations.
         SPDirectionsManager will attempt to find routes for each destination.
         */
        
        let request = SPDirectionsRequest(location: startLocation, destinations: [endLocation])
        
        /**
         Calculate directions (routes). This requires an internet connection if route information
         has not yet been downloaded onto device. You can however bundle route information into your
         app if necessary. For offline routes contact support@steerpath.com
         
         Currently the SDK supports only routes inside buildings.
         */
        
        SPDirectionsManager.sharedInstance().getDirectionsWith(request) { (response, error) in
            guard let routeCount = response?.routes().count else { return }
            //Check if we found any routes
            if (routeCount > 0) {
                /**
                 Draws first route that was found onto map.
                 Currently SPMapView supports only drawing a single route at a time
                 and it will replace the old route if necessary.
                 */
                
                let route = response?.routes().first
                
                self.mapView.setRoute(route!)
            }
        }
        
    }
    
    // MARK: IBAction
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

// MARK: SPLocationManagerDelegate methods

extension StaticRouteViewController: SPLocationManagerDelegate {
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        self.mapView.setCurrentLocation(location)
    }
    
    func spLocationManager(_ manager: SPLocationManager, didLoadNDD success: Bool, error: Error?) {
        //If success == true, indoor positioning is available.
    }
}

// MARK: SPMapViewDelegate

extension StaticRouteViewController: SPMapViewDelegate {
    /**
     This determines the alpha for the route line among other things.
     */
    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 1.0
    }
    
    /**
     This determines the color of the route line among other things.
     */
    
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return UIColor.blue
    }
    
    /**
     This can be used to determine the width of the route line on the map.
     */
    
    func mapView(_ mapView: MGLMapView, lineWidthForPolylineAnnotation annotation: MGLPolyline) -> CGFloat {
        return 6.0
    }
    
    /**
     This will create an annotation view.
     The map view will internally add annotations onto the route so you
     will need to implement this if you want to use the route annotations provided by the SDK.
     */
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        
        annotationView?.annotationColor = .blue
        
        return annotationView
    }
}

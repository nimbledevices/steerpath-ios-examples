//
//  FeatureSelectionViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// Dependencie

import Foundation
import Steerpath

// FeatureSelectionViewController implementation

class FeatureSelectionViewController: UIViewController {
    
    // IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    private var locationManager: SPLocationManager!
    
    // MARK: -Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Instantiate locationManager
        
        locationManager = SPLocationManager(delegate: self)
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
    }
    
    // IBActions
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    // MARK: Utils
    
    private func addAnnotationFor(localRef: String) {
        // Query with 'localRef' as tag
        
        let query = SPQuery()
        query.tags = [localRef]
        
        SPMetaService.sharedInstance().getPointsOfInterest(query) { (points, error) in
            guard let point = points?.first else { return }
            
            let annotation = MGLPointAnnotation()
            annotation.coordinate = point.coordinate()
            annotation.title = point.title()
            
            let currentAnnotationsInMapView = self.mapView.annotations ?? [MGLPointAnnotation]()
            
            var isAnnotationAdded = false
            
            for currentAnnotation in currentAnnotationsInMapView {
                if (currentAnnotation.title == annotation.title) {
                    isAnnotationAdded = true
                }
            }
            
            if currentAnnotationsInMapView.count == 0 || !isAnnotationAdded {
                self.mapView.addAnnotation(annotation, floor: point.floor())
            }
        }
    }
}

// MARK: SPMapViewDelegate

extension FeatureSelectionViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        self.mapView.setCenter(annotation.coordinate, animated: true)
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        annotationView?.annotationColor = .red
        return annotationView
    }
    
    func mapView(_ mapView: SPMapView, didSelect features: [MGLFeature]) {
        for feature in features {
            if let localRef = feature.attributes["localRef"] as? String {
                self.addAnnotationFor(localRef: localRef)
            }
        }
    }
}

// MARK: SPLocationManager

extension FeatureSelectionViewController: SPLocationManagerDelegate {
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        self.mapView.setCurrentLocation(location)
    }
}

//
//  BackgroundUpdateViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 19/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: -Dependencie

import Foundation
import Steerpath

// MARK: BackgroundUpdateViewController Implementation

class BackgroundUpdateViewController: UIViewController {
    
    // MARK: -IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    // MARK: -Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: Utils
    
    private func setupView() {
        centerMap()
        
        //Start the background location manager
        
        SteerpathBackgroundLocationManager.shareInstance.start()
        /**
         In this example we listen to notifications that are sent from the 'SteerpathBackgroundLocationManager'.
         SteerpathBackgroundLocationManager will stay alive even when this view controller is not active and will provide a bluetooth position
         when the application is running in the background.
         */
        
        NotificationCenter.default.addObserver(self, selector: #selector(locationNotification(notification:)), name: NSNotification.Name(rawValue: kSPLocationUpdatedNotification), object: nil)
    }
    
    @objc private func locationNotification(notification: Notification) {
        if notification.name.rawValue == kSPLocationUpdatedNotification {
            
            let location = notification.object as! SPLocation
            
            self.mapView.setCurrentLocation(location)
        }
    }
    
    private func centerMap() {
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
    }
    
    // MARK: IBActions
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

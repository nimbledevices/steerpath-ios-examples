//
//  DebugLocationViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 15/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: Dependencies

import Foundation
import Steerpath

// DebugLocationVC Implementation

class DebugLocationViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    private var locationManager: SPLocationManager!
    
    // MARK: - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /**
         Centers map onto specific coordinates and zoom level.
         Zoom level 0 is the whole world map.
         */
        
        let centerCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949)
        
        self.mapView.setCenter(centerCoordinate, animated: true)
        
        /**
         Map view will start to follow users location.
         */
        
        self.mapView.setUserTrackingMode(MGLUserTrackingMode.follow, animated: true)
        
        /**
         Initializes location manager. Automatically starts bluetooth positioning if bluetooth is turned on.
         If bluetooth is not on, the app will ask the user to turn on bluetooth.
         Also asks permission to use iOS Location Services (GPS).
         
         When users position changes, a callback will be made to the delegate method
         spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation)
         */
        
        self.locationManager = SPLocationManager(delegate: self)
        
        /**
         We use a debug position instead of actually using a bluetooth or GPS position.
         This is useful for demonstration purposes or for example testing navigation/routes.
         
         After the debug location is set, the following delegate method will always return it:
         spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation)
         */
        
        let debugCoordinate = CLLocationCoordinate2DMake(60.167064, 24.937949)
        
        let location = SPLocation(coordinate: debugCoordinate, floor: 8, horizontalAccuracy: 1, course: 0, source: .bluetooth)
        
        /**
         If you want to limit the user blue dot to a specific floor in a building
         you need to also set a building identifier to the location. Otherwise the
         location will be visible on all floors when viewing indoor maps.
         
         The unique building identifier can be found by fetching buildings via SPMetaService method
         'getBuildings' and checking the SPGeoJson identifier for that particular building.
         
         */
        
        location.building = "574ec89bbb87115f2fa0c75a";    //example of a unique identifier
        
        self.locationManager.setDebugLocation(location)
        
        setupViews()
    }
    
    // MARK: Setup methods
    
    private func setupViews() {
        
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}

// MARK: SPLocationManagerDelegate

/** This is called every time users location changes */

extension DebugLocationViewController: SPLocationManagerDelegate {
    func spLocationManager(_ manager: SPLocationManager, didUpdate location: SPLocation) {
        
        /**
         Pass the location object to the map to show users location.
         */
        
        self.mapView.setCurrentLocation(location)
    }
}

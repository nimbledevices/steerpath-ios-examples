//
//  CustomAnnotationViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 15/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// Dependencies

import Foundation
import Steerpath

// CustomAnnotationViewController Implementation

class CustomAnnotationViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    // MARK: - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
        addAllToilets()
        
        addKitchen()
        
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: Utils
    
    private func addAllToilets() {
        //First we remove all the old annotations we've placed onto the map
        
        if let annotationsToRemove = self.mapView.annotations {
            self.mapView.removeAnnotations(annotationsToRemove)
        }
        
        //Fetch toilet points and add them to the map on the correct floors
        
        let query = SPQuery()
        query.tags = ["Toilet", "service_toilet"]
        
        SPMetaService.sharedInstance().getPointsOfInterest(query) { (points, error) in
            /**
             Add annotations for each point of interest that has
             an annotation and a floor associated.
             */
            
            guard let points = points else { return }
            
            for point in points {
                if let name = point.properties()["localRef"] as? String {
                    let customPointAnnotation = CustomPointAnnotation()
                    customPointAnnotation.coordinate = point.coordinate()
                    customPointAnnotation.text = name
                    
                    self.mapView.addAnnotation(customPointAnnotation, floor: point.floor())
                }
            }
        }
    }
    
    private func addKitchen() {
        //First we remove all the old annotations we've placed onto the map
        
        if let annotationsToRemove = self.mapView.annotations {
            self.mapView.removeAnnotations(annotationsToRemove)
        }
        
        //Fetch toilet points and add them to the map on the correct floors
        
        let query = SPQuery()
        query.tags = ["Kitchen"]
        
        SPMetaService.sharedInstance().getPointsOfInterest(query) { (points, error) in
            /**
             Add annotations for each point of interest that has
             an annotation and a floor associated.
             */
            
            guard let points = points else { return }
            
            for point in points {
                if let name = point.properties()["localRef"] as? String {
                    let customPointAnnotation = CustomPointAnnotation()
                    customPointAnnotation.coordinate = point.coordinate()
                    customPointAnnotation.text = name
                    
                    self.mapView.addAnnotation(customPointAnnotation, floor: point.floor())
                }
            }
        }
    }
    
}

// MARK: SPMapViewDelegate

extension CustomAnnotationViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if annotation is CustomPointAnnotation {
            
            let customPointAnnotation = annotation as! CustomPointAnnotation
            
            let reuseIdentifier = "customReuseIdentifier"
            
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? CustomAnnotationView
            
            if !(annotationView != nil) {
                annotationView = CustomAnnotationView(reuseIdentifier: reuseIdentifier)
                
                annotationView?.onButtonClicked =  { (text, clickedAnnotation) in
                    print("Clicked button with text: \(text)")
                }
            }
            annotationView?.setText(text: customPointAnnotation.text ?? "")
            
            return annotationView
            
        } else {
            let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
            annotationView?.annotationColor = .red // Set annotation color
            return annotationView
        }
    }
}

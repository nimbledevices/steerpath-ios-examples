//
//  MapViewController.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 20/02/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit
import Steerpath

//MARK: - MapViewController Class Implementation

class MapViewController: UIViewController, SPMapViewDelegate {
    
    //MARK: - Properties
    
    @IBOutlet fileprivate var mapView : SPMapView!

    //MARK: - Object Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /**
            Set the map center with a GPS coordinate and zoom level.
            The bigger the zoom level the more detailed the map is.
            Zoom level 0 = world map.
        */
        let coordinate = CLLocationCoordinate2DMake(60.220946, 24.812392)
        let zoom = 20.0
        self.mapView.setCenter(coordinate, zoomLevel: zoom, animated: false)
    }
    
    // MARK: IBActions

    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

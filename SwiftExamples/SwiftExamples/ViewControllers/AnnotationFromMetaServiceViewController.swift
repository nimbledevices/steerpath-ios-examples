//
//  AnnotationFromMetaServiceViewController.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: - Dependencies

import Foundation
import Steerpath

// MARK: - AnnotationFromMetaServiceViewController implementation

class AnnotationFromMetaServiceViewController: UIViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var mapView: SPMapView!
    
    // MARK: - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
        /**
         Create a query object for fetching points of interest data.
         
         Added following parameters to query:
         - floor has to be equal to 2 (0 is ground floor)
         - tag "toilet" "service_toilet"
         */
        
        let query = SPQuery()
        query.tags = ["Toilet", "service_toilet"]
        
        /**
         Requesting point of interest data from Steerpath Meta Service. Requires internet
         connection when first time downloading a particular set of data. Will cache data locally.
         
         To use the SPMetaService you must have a Steerpath API key.
         See SPAccountManager.h for more information.
         */
        
        SPMetaService.sharedInstance().getPointsOfInterest(query) { (points, error) in
            guard let points = points else { return }
            
            /**
             Add annotations for each point of interest that has
             an annotation and a floor associated.
             */
            
            for point in points {
                if let annotation = point.annotation(), let floor = point.floor() {
                    self.mapView.addAnnotation(annotation, floor: floor)
                }
            }
        }
        
    }
    
    // MARK: IBActions
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

// MARK: - SPMapViewDelegate methods

extension AnnotationFromMetaServiceViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        
        annotationView?.annotationColor = .red // Set annotation color
        
        return annotationView
    }
    // This is called when user selects an annotations
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        self.mapView.setCenter(annotation.coordinate, animated: true)
    }
}

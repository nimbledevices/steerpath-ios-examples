//
//  FloorSpecificAnnotations.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

// MARK: - Dependencies

import Foundation
import Steerpath

// MARK: - FloorSpecificAnnotationViewController implementation

class FloorSpecificAnnotationViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: SPMapView!
    
    
    // MARK: - Object lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Center the map around the building
        
        let buildingQuery = SPQuery()
        
        // Get list of building for current API key
        
        SPMetaService.sharedInstance().getBuildings(buildingQuery) { (buildings, error) in
            guard let response = buildings else { return }
            
            if let firstBuilding = response.first {
                let boundingBox = firstBuilding.boundingBox()
                let mglBoundingBox = MGLCoordinateBounds(sw: boundingBox.southWest(), ne: boundingBox.northEast())
                let edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
                self.mapView.setVisibleCoordinateBounds(mglBoundingBox, edgePadding: edgeInsets, animated: false)
            }
        }
        
        let toiletQuery = SPQuery()
        toiletQuery.tags = ["Toilet", "service_toilet"]
        
        // Get list of toilet inside the building for current API Key
        
        SPMetaService.sharedInstance().getPointsOfInterest(toiletQuery) { (points, error) in
            guard let points = points else { return }
            
            // Then create point annotation with each returned value and add to mapView
            
            for point in points {
                let annotation = MGLPointAnnotation()
                annotation.coordinate = point.coordinate()
                
                self.mapView.addAnnotation(annotation, floor: NSNumber(integerLiteral: 2))
            }
        }
    }
    
    // MARK: - IBActions
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

// MARK: - SPMapViewDelegate Implementation

extension FloorSpecificAnnotationViewController: SPMapViewDelegate {
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        /**
         This will dequeue an annotation view provided by the Steerpath SDK
         */
        let annotationView = self.mapView.dequeueAnnotationView(for: annotation)
        annotationView?.annotationColor = .red
        return annotationView
        
    }
    
    // This is called when user selects an annotation
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        self.mapView.setCenter(annotation.coordinate, animated: true)
    }
}

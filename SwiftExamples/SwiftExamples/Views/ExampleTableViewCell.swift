//
//  ExampleTableViewCell.swift
//  SwiftExamples
//
//  Created by Jussi Laakkonen on 20/02/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

//MARK: - Dependencies

import UIKit



//MARK: - ExampleTableViewCell Class Implementation

class ExampleTableViewCell: UITableViewCell {
    @IBOutlet public var titleLabel : UILabel!
    static let identifier = "ExampleTableViewCellIdentifier"
    static let nibName = "ExampleTableViewCell"
    
}

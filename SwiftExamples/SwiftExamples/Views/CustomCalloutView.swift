//
//  CustomCalloutView.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 16/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

import Foundation
import Steerpath

class CustomCalloutView: UIView, MGLCalloutView {
    
    // MARK - Properties
    
    var representedObject: MGLAnnotation
    
    var leftAccessoryView: UIView = UIView()
    
    var rightAccessoryView: UIView = UIView()
    
    weak var delegate: MGLCalloutViewDelegate?
    
    var dismissesAutomatically: Bool {
        return true
    }
    
    var isAnchoredToAnnotation: Bool {
        return false
    }
    
    // MARK: - Subviews
    
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        return label
    }()
    
    // MARK: -Object lifecycle
    
    required init(annotation: CustomPointAnnotation) {
        self.representedObject = annotation
        
        super.init(frame: CGRect(origin: .zero, size: CGSize(width: 150, height: 45)))
        
        self.textLabel.text = self.representedObject.title ?? ""
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupView() {
        self.backgroundColor = .white
        
        // Add to view's hierrachy
        
        self.addSubview(textLabel)
        
        // Constraint subview
        
        self.textLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.textLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.textLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.textLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
    }
    
    // MGLCalloutView protocol methods
    
    func presentCallout(from rect: CGRect, in view: UIView, constrainedTo constrainedRect: CGRect, animated: Bool) {
        
        self.center = view.center.applying(CGAffineTransform(translationX: 0, y: -60))
        
        view.addSubview(self)
    }
    
    func dismissCallout(animated: Bool) {
        self.removeFromSuperview()
    }
}

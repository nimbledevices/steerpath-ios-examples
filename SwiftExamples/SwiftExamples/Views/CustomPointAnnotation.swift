//
//  CustomPointAnnotation.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 15/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

import Foundation
import Steerpath

class CustomPointAnnotation: MGLPointAnnotation {
    var text: String?
    var location: SPLocation?
}

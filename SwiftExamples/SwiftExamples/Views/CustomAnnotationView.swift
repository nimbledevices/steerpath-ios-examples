//
//  CustomAnnotationView.swift
//  SwiftExamples
//
//  Created by Nguyen Ba Long on 15/11/2018.
//  Copyright © 2018 Steerpath. All rights reserved.
//

import Foundation
import Steerpath

class CustomAnnotationView: MGLAnnotationView {
    
    private var button: UIButton!
    private var textLabel: UILabel!
    private var currentText: String?
    
    public var onButtonClicked: ((String?, CustomPointAnnotation?) -> Void)?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        /**
         Here you would add custom subviews, load nibs etc.
         and add them as a subview.
         
         Currently Mapbox does not have an API for loading reusable annotation views
         straight from xib files.
         */
        
        guard let customView = Bundle.main.loadNibNamed("CustomAnnotationView", owner: self, options: nil)?.first as? UIView else {
            return
        }
        
        self.button = customView.viewWithTag(450) as? UIButton
        
        self.button.addTarget(self, action: #selector(handleBtnPressed), for: .touchUpInside)
        
        self.button.clipsToBounds = true
        
        self.textLabel = customView.viewWithTag(451) as? UILabel
        
        self.addSubview(customView)
        
        // Set frame to whatever the size of the custom subviews are
        
        self.frame = CGRect(x: 0, y: 0, width: customView.frame.width, height: customView.frame.height)
        self.scalesWithViewingDistance = false
        self.isEnabled = true
        
        self.clipsToBounds = true
        self.translatesAutoresizingMaskIntoConstraints = false
        
        // Annotation view constraints
        
        self.heightAnchor.constraint(greaterThanOrEqualTo: customView.heightAnchor, multiplier: 1, constant: 0).isActive = true
        self.widthAnchor.constraint(greaterThanOrEqualTo: customView.widthAnchor, multiplier: 1, constant: 0).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: IBAction
    
    @objc private func handleBtnPressed() {
        
        if let onButtonClicked = self.onButtonClicked {
            onButtonClicked(self.currentText, self.annotation as? CustomPointAnnotation)
        }
        
    }
    
    // MARK: Public methods
    
    public func setText(text: String) {
        self.currentText = text
        self.textLabel.text = text
    }
    
}
